# myistra-iframe-api

This is an example showing the usage of the myIstra iFrame API:
* a minimalist HTTP server will be started using [Express](http://expressjs.com/)
* it simulates you business app, running by default at ``http://localhost:5000`` (it uses [Tacit](https://github.com/yegor256/tacit), the CSS framework for dummies)
* browsing the page shows side by side:
  * a minimalistic business web app, i.e.: your actual webapp that you want to integrate myIstra with
  * your myIstra instance, loaded in the right side panel (see _§ Configuring the demo_, below)

# Quick start

Clone the repo:

    git clone git@gitlab.com:centile-pub/myistra-iframe-api

Install the project dependencies:

    npm install
    
Launch the server server:

    npm run start

You may want to change the default port by using the PORT environment variable
    
    export PORT=8888 && npm run start
    
You may also use the DEBUG environment variable, if you want the server process to log each request:

    export DEBUG=true && npm run start
   
# Starting the demo

To browse the example, point your browser to

   http://localhost:5000
   
And... You you will see it will not work: this is because, for the sake of the plug'n'playness of this demo, you must give your myIstra URL in the query string as follows:

   http://localhost:5000/?myistra=https://myistra.mysuperdomain.com

(of course, you must use your own myIstra URL instead of using ``https://myistra.mysuperdomain.com`` !)

# Browser security issues

Depending of the browser in use, the specified myIstra will, or will not, load in the iframe: this is because the demo runs over a HTTP localhost document, while your myIstra instance will most probably run over a HTTPs (TLS-secured) server. In some cases, it will load myIstra, but won't allow the iFrame API to be used, in other cases the browser will not even load myIstra.

At the time of writing, Chrome 79.0 seems to be permissive enough to let the demo run as is. Firefox and Safari are not.

Coping with the security issues of a localhost server is left as an exercice for the reader :-)

# Using the example

The example will basically log each activity seen on the iFrame API:
1. All events emitted by miIstra will be shown
2. All actions submitted to myIstra will be shown as well

Just play with the action in the main web app, and perorm some telephony oriented actions in myIstra to see the results. Also check the source code :-)

# Advanced example

There is also a more exhaustive example that shows more actions and events, browse: 

   http://localhost:5000/advanced.html

However, be warned it is less polished :-)
It must also be edited first to reflect your myIstra URL to load in the iFrame (c.f. the iframe line, in [./www/static/advanced.hml](./www/static/advanced.hml)

It is more interesting since it shows more operations you can submit to myIstra, and shows more usage of the events (like the list of calls, ACD Groups, ACD pauses, Caller IDs).

# More info?

Please refer to Centile's extranet, in order to get the technical infomation about the feature, how to configure it, and how to implement your own yellow page server for your use case.

In case of doubt, ask us through the usual channels, or ultimately at https://centile.com/contact-page :-)
