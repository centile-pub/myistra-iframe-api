// Our modules dependencies
const path = require('path');
const express = require('express');
const fs = require('fs');

// Some constants
const e = express();

// A small helper function to transform a request into a loggable string
const logRequest = (r) => {
  if (process.env.DEBUG == "true") {
    console.log(`DEBUG: ${r.method} ${r.url} with headers:\n${JSON.stringify(r.headers, '\n', 4)}`);
  }
};

// Configure our server
e
  .use(express.static(path.join(__dirname, '../www/static')))
  .set('views', path.join(__dirname, '../www/views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => { logRequest(req); res.render('index'); });

// Export our configured server
module.exports = e;
