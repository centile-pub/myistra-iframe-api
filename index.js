// Our modules dependencies
const server = require('./lib/server');

// Starts the server
const PORT = process.env.PORT || 5000;
server.listen(PORT, () => {
  console.log(`> Listening on port ${PORT}`);
});
